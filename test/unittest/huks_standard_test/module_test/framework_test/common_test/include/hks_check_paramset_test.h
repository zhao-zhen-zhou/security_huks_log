/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef HKS_CHECK_PARAMSET_TEST_H
#define HKS_CHECK_PARAMSET_TEST_H

namespace Unittest::HksFrameworkCommonCheckParamsetTest {
/**
 * HksCoreCheckMacParams
 */
int HksCheckParamsetTest001(void);
int HksCheckParamsetTest002(void);
int HksCheckParamsetTest003(void);
int HksCheckParamsetTest004(void);
int HksCheckParamsetTest005(void);
int HksCheckParamsetTest006(void);
int HksCheckParamsetTest007(void);

/**
 * HksCoreCheckDeriveKeyParams
 */
int HksCheckParamsetTest008(void);
int HksCheckParamsetTest009(void);
int HksCheckParamsetTest010(void);
int HksCheckParamsetTest011(void);
int HksCheckParamsetTest012(void);
int HksCheckParamsetTest013(void);
int HksCheckParamsetTest014(void);
int HksCheckParamsetTest015(void);
int HksCheckParamsetTest016(void);
int HksCheckParamsetTest017(void);
int HksCheckParamsetTest018(void);

/**
 * HksLocalCheckCipherParams
 */
int HksCheckParamsetTest019(void);
int HksCheckParamsetTest020(void);
int HksCheckParamsetTest021(void);
int HksCheckParamsetTest022(void);
int HksCheckParamsetTest023(void);
int HksCheckParamsetTest024(void);
int HksCheckParamsetTest025(void);
int HksCheckParamsetTest026(void);
int HksCheckParamsetTest027(void);
int HksCheckParamsetTest028(void);
int HksCheckParamsetTest029(void);
int HksCheckParamsetTest030(void);
int HksCheckParamsetTest031(void);
int HksCheckParamsetTest032(void);
int HksCheckParamsetTest033(void);
int HksCheckParamsetTest034(void);
int HksCheckParamsetTest035(void);
int HksCheckParamsetTest036(void);
int HksCheckParamsetTest037(void);
int HksCheckParamsetTest038(void);
}
#endif // HKS_CHECK_PARAMSET_TEST_H
